pragma solidity >=0.4.25 <0.6.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/IntelloCoin.sol";

contract TestMetacoin {

  function testInitialBalanceUsingDeployedContract() private {
    IntelloCoin meta = IntelloCoin(DeployedAddresses.IntelloCoin());

    uint expected = 10000;

    Assert.equal(meta.balanceOf(msg.sender), expected, "Owner should have 10000 IntelloCoin initially");
  }

  function testInitialBalanceWithNewIntelloCoin() private {
    IntelloCoin meta = new IntelloCoin();

    uint expected = 10000;

    Assert.equal(meta.balanceOf(msg.sender), expected, "Owner should have 10000 IntelloCoin initially");
  }

}
