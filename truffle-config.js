// Allows us to use ES6 in our migrations and tests.
require("babel-register");
const path = require("path");

module.exports = {
	contracts_build_directory: path.join(__dirname, "build"),
};
