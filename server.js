const express = require("express");
const app = express();
const port = 3000 || process.env.PORT;
const Web3 = require("web3");
const truffle_connect = require("./connection/app.js");
const bodyParser = require("body-parser");
const mustache = require("mustache-express");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
app.use("html", mustache());

app.set("view engine", "html");
app.set("views", __dirname + "/views");

app.get("/", (req, res) => {
	truffle_connect.start((ans) => {
		res.send(ans);
	});
});
//app.use("/", express.static("public_static"));







app.get("/getAccounts", (req, res) => {
	console.log("**** GET /getAccounts ****");
	truffle_connect.start(function(answer) {
		res.send(answer);
	});
});

app.post("/balanceOf", (req, res) => {
	console.log("**** GET /balanceOf ****");
	console.log(req.body);
	let currentAccount = req.body.account;

	truffle_connect.refreshBalance(currentAccount, answer => {
		let account_balance = answer;
		truffle_connect.start(function(answer) {
			// get list of all accounts and send it along with the response
			let all_accounts = answer;
			response = [account_balance, all_accounts];
			res.send(response);
		});
	});
});

app.post("/sendCoin", (req, res) => {
	console.log("**** GET /sendCoin ****");
	console.log(req.body);

	let amount = req.body.amount;
	let sender = req.body.sender;
	let receiver = req.body.receiver;

	truffle_connect.sendCoin(amount, sender, receiver, balance => {
		res.send(balance);
	});
});

app.listen(port, () => {
	// fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
	truffle_connect.web3 = new Web3(
		new Web3.providers.HttpProvider("http://127.0.0.1:7545")
	);

	console.log("Express Listening at http://localhost:" + port);
});
